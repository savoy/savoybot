# SavoyBot ![version](https://img.shields.io/badge/python-3.4.2%2B-de935f.svg)
##### Simple Discord bot

A simple and multifunctional Discord bot that started off as a way for a hobbyist programmer to actually practice Python.
Any advice or code improvement suggestions are welcome! As it's my first Python project outside of simple 5-10 line scripts for computer maintenance, I know there are plenty of areas where I can streamline and improve the code.


## Features

#### Serious

- [x] Output weather based off of given location. 
- [x] Searches DuckDuckGo and brings up results using [ddgr](https://github.com/jarun/ddgr).
- [x] Pull LastFM/LibreFM statistics, filterable with options.
- [ ] Image upload through Framapic
- [x] SQLite3 database for setting bot options e.g. location for weather, usernames, command settings.
- [ ] General admin settings e.g. user info, message deletion, muting, kicking.
- [x] A block list to limit users from using the bot if need be.

#### Fluff

- [x] Print 'copypastas' while replacing keywords with user choice.
- [x] Alternates text capitalization for supplied message or for copypastas.
- [x] Implement `Cowsay` as a command.

## Commands

Command | Options | Usage
--------|---------|------
.s man | - | Prints Github link and list of commands with short descriptions. Specific command options can be found with `.s cmd_name --help or -h`.
.s save | [-w], [-f] | Saves the given user data for use in commands.
.s pasta | _view help_ | Prints chosen copypasta, substituting keywords with user-supplied ones `.s pasta -a Github`.
.s weather | - | Prints weather output for given location or saved location by using your saved unit of measurement as a modifier.
.s sponge | _see pasta options_ | Prints copypastas while alternating capitalization or user-supplied text `.s pasta -l Savoy Bot` or `.s pasta Hello World!`
.s ddg | [-j], [-w] | Searches DuckDuckGo for given search term and prints top 10 results `.s ddg Debian`. [-j] will show the first result w/site embed while [-w] will search a specific site by either its domain or a hard-coded shortcut `.s ddg -w a HDMI cable` or `.s ddg -j -w yt Longtime Sunshine`
.s fm | - | Prints output for user displaying Now Playing, most recent scrobble, and top 10 artist for last week. 
.s hs | - | Shows daily horoscope.


## Installation

#### Prerequisites
- GNU/Linux not required, but recommended.
- Python 3.4.2+ (tested with 3.5.3).
- Pip
- Pipenv recommended.
- Git
- [ddgr](https://github.com/jarun/ddgr) (Available through most distro repositories, also available as a snap).

#### Instructions
```bash
$ cd desired_directory/
$ git clone https://gitlab.com/savoyroad/savoybot # Clone bot repo
$ pip3 install --user pipenv                      # If pipenv not yet installed
$ pipenv --three                                  # Initalizes Python3 virtual environment
$ pipenv shell
$ pipenv install
### Before running, read through and modify config file (changing name from template_config.py to config.py)
###to make sure options are set for your purposes e.g bot token, file paths, etc.
$ ./savoybot.py
```
#### Install as a systemd service
In order for the bot to run without interruption, consider running it as a systemd service. Save the following as `/lib/systemd/system/discord_bot.service`.
```bash
[Unit]
Description=SavoyBot
After=multi-user.target

[Service]
User=                                             # Username for the bot to run under, usually yours
Group=                                            # Group for the bot to run under, usually yours  
Restart=always
Type=idle
WorkingDirectory=                                 # Absolute path for savoybot/
ExecStart=/usr/local/bin/pipenv run python3 $WORKING_DIRECTORY_HERE
StandardOutput=null

[Install]
WantedBy=multi-user.target
```

Shoutout to the competing FossBot by [@emeraldsanto](https://github.com/emeraldsanto/FossBot).
