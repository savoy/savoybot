#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018-2019 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

# Import libraries used in __main__
import discord
from discord.ext import commands
from base.config import config as config
from base import parser
from base import exceptions
from base import output
from base import server
import sys

# Get configuration
conf = config()

# Token selection
try:
    token = conf['token'][sys.argv[1]]
except IndexError:
    token = conf['token']['main']
except KeyError:
    raise ValueError(
        'Please choose correct token from config.yaml or leave blank')

# Setup of Bot
bot = commands.Bot(command_prefix='.')

# Logging
if conf['logging']:
    import logging
    logger = logging.getLogger('discord')
    logger.setLevel(logging.DEBUG)
    handler = logging.FileHandler(filename='discord.log', encoding='utf-8',
            mode='w')
    handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s:%(message)s'))
    logger.addHandler(handler)

@bot.event
async def on_ready():
    # Logs the bot into Discord
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')
    await bot.change_presence(activity=discord.Game(conf['status']))


#@bot.event
#async def on_member_update(before, after):
#    if after.id not in conf['wheel']:
#        pass
#    else:
#        old = before.roles
#        new = after.roles
#        channel = before.guild.channels[0]
#        if [s.name == 'supermute' for s in new if s not in old]:
#            await channel.send(
#                f'{before.name} has been supermuted. This will only be '
#                'displayed once. Reminder that mods are враги народа and '
#                'would accept fascists in chat rather than ban them. They '
#                'consistently display the most centrist, spineless, and '
#                'dogmatic decision making that flies against everything in '
#                'their "rules", exhibiting favoritism to any and all that '
#                'share their lack of ideology. Deleting this message is an '
#                'act of censorship. Stand in solidarity with your fellow '
#                'users.')

@bot.event
async def on_member_join(member):
    server = member.guild
    if server.id not in conf['group'].keys():
        pass
    else:
        for key, chan in enumerate(server.channels):
            if chan.name == 'gate':
                channel = server.channels[key]
                break
        for msg in conf['group'][server.id]:
            await channel.send(msg.format(member.mention))

@bot.event
async def on_member_ban(guild, user):
    if user.id not in conf['wheel']:
        pass
    else:
        channel = guild.channels[0]
        await channel.send(
            f'{user.name} has been banned. This will only be displayed once. '
            'Reminder that mods are враги народа and would accept fascists in '
            'chat rather than ban them. They consistently display the most '
            'centrist, spineless, and dogmatic decision making that '
            'flies against everything in their "rules", exhibiting favoritism '
            'to any and all that share their lack of ideology. Deleting this '
            'message is an act of censorship. Stand in solidarity with your '
            'fellow users.')


#@bot.event
#async def on_message(message):
#    '''Replies with an emoji if mentioned'''
#    if message.author == bot.user:
#        return
#    if message.content.startswith(bot.user.mention):
#        output = [
#            value for key, value in options.emoji.items() if value in
#            message.content
#        ]
#        if output:
#            output = ' '.join(output)
#            msg = '{}, {}'.format(message.author.mention, output)
#            await ctx.send_message(message.channel, msg)
#        else:
#            return
#
#    if 'bobby b' in message.content.lower():
#        sql.cur.execute(
#            "SELECT quote FROM quotes WHERE keyword=?", ['bobby b']
#        )
#        results = sql.cur.fetchall()
#        row = random.choice(results)
#        qt = row[0]
#
#        msg = '{}, {}'.format(message.author.mention, qt)
#        await ctx.send_message(message.channel, msg)
#
#    await bot.process_commands(message)


@bot.command(pass_context=True)
async def s(ctx, *message):
    p = parser.Parser(ctx, message, conf)
    # Gets output, and sends it either as an embed or plain text
    try:
        out = getattr(p, p.command)()
        if p.command in dir(server):
            try:
                await getattr(server, p.command)(ctx, bot, out, p)
            except (AttributeError, TypeError):
                try:
                    await ctx.send(embed=out)
                except AttributeError:
                    await ctx.send(out)

        try:
            await ctx.send(embed=out)
        except AttributeError:
            await ctx.send(out)

    # Below are list of general exceptions for the bot to handle
    except discord.errors.HTTPException:
        p.parsed['error'] = True
        data = {
            'header':   'Something happened...',
            'value' :   'Discord error, go back to your drinks.'
        }
        out = output.Output(p.parsed, data).error()
        try:
            await ctx.send(embed=out)
        except AttributeError:
            await ctx.send(out)
    except exceptions.AttributeError:
        p.parsed['error'] = True
        data = {
            'header':   'Please set your attribute',
            'value' :   'Set your command attribute or run with your target'
        }
        out = output.Output(p.parsed, data).error()
        try:
            await ctx.send(embed=out)
        except AttributeError:
            await ctx.send(out)
    except exceptions.ArgError:
        p.parsed['error'] = True
        data = {
            'header':   'Incorrect command argument',
            'value' :   "Run the command with '-h' to view correct arguments."
        }
        out = output.Output(p.parsed, data).error()
        try:
            await ctx.send(embed=out)
        except AttributeError:
            await ctx.send(out)
    except exceptions.CommandError:
        p.parsed['error'] = True
        data = {
            'header':   'Incorrect command',
            'value' :   "Choose a correct command. Run '.s man' for options."
        }
        out = output.Output(p.parsed, data).error()
        try:
            await ctx.send(embed=out)
        except AttributeError:
            await ctx.send(out)
    except exceptions.PermissionError:
        p.parsed['error'] = True
        data = {
            'header':   'Incorrect permissions',
            'value' :   'You are not authorized to perform this command.'
        }
        out = output.Output(p.parsed, data).error()
        try:
            await ctx.send(embed=out)
        except AttributeError:
            await ctx.send(out)
    except exceptions.NotFoundError:
        p.parsed['error'] = True
        data = {
            'header':   'File Not Found',
            'value' :   'The queried item does not exist.'
        }
        out = output.Output(p.parsed, data).error()
        try:
            await ctx.send(embed=out)
        except AttributeError:
            await ctx.send(out)
    except exceptions.GulagError:
        p.parsed['error'] = True
        data = {
            'header':   'Нет Разрешения',
            'value' :   'Ты в гулаг, классовый враг'
        }
        out = output.Output(p.parsed, data).error()
        try:
            await ctx.send(embed=out)
        except AttributeError:
            await ctx.send(out)
    except Exception as e:
        if token == conf['token']['dev']:
            print('DEBUGGING EXCEPTION')
            raise e
        else:
            p.parsed['error'] = True
            data = {
                'header':   'An unknown error occured',
                'value' :   e
            }
            out = output.Output(p.parsed, data).error()
        try:
            await ctx.send(embed=out)
        except AttributeError:
            await ctx.send(out)

bot.run(token)
