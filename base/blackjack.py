#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018-2019 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If NOT, see <http://www.gnu.org/licenses/>.

import random
import datetime as d
from base import exceptions
from base import sql
from ast import literal_eval

def update(deck_id: int, col, value):
    cols = ['dealt', 'playing', 'finished', 'players', 'deck']
    with sql.Sql() as conn:
        cur = conn.cursor()
        if type(col) == list and type(value) == list:
            for c, v in zip(col, value):
                cur.execute(
                    f"UPDATE Blackjack SET {c}=? WHERE id=?",
                    [v, deck_id])
                conn.commit()
        else:
            cur.execute(
                f"UPDATE Blackjack SET {col}=? WHERE id=?", [value, deck_id])
        conn.commit()

def check_locked(user_id, user_name, update: int=None):
    with sql.Sql() as conn:
        cur = conn.cursor()
        if update:
            cur.execute(
                "INSERT INTO locked (id, name, locked) VALUES(?, ?, ?) "
                "ON CONFLICT (id) DO UPDATE SET name=excluded.name, locked=?",
                [user_id, user_name, update, update])
            conn.commit()
            return update
        else:
            cur.execute(
                "SELECT locked FROM Attributes where id=?", [user_id])
            results = cur.fetchall()[0]
            return results

def card_parser_helper(player):
    cards = {
        'Jack'  :   11,
        'Queen' :   11,
        'King'  :   11,
        'Ace'   :   [1, 11]
    }

    aces = []
    try:
        players['total'] += card[1]
    except TypeError:
        try:
            players['total'] += cards[card[1]]
        except TypeError:
            aces.append(card)

if aces:
    if players['total'] + 11 >= 21:

def card_parser(players: dict, single: bool=False):
    try:
        for player in players.values():
            player['total'] = card_parser_helper(player)
    except TypeError:
        players['total'] = card_parser_helper(player)

def check_twentyone(house, players: dict):
    house_points = sum(players[house]['hand'])
    if house_points 
    for key, value in players.items():
        if sum(values['hand'])

class Deck():
    def __init__(self, ctx, deck_id: int=None, bet: int=None):
        self.ctx = ctx
        self.house = self.ctx.bot.id
        if not deck_id:
            if not bet or bet <= 0:
                raise exceptions.BetError
            self.deck = []
            self.initiator = self.ctx.message.author.id
            if check_locked(self.initiator, self.ctx.message.author.name):
                raise exceptions.PlayerError
            today = dt.datetime.today()
            for suit in ['Clubs', 'Diamonds', 'Hearts', 'Spades'] * 4:
                for rank in ['Ace', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'Jack',
                             'Queen', 'King'] * 4:
                    self.deck.append((suit, rank))

            self.id = int(
                str(self.initiator.id) + today.strftime('%y%m%d%H%M%S'))

            with sql.Sql() as conn:
                cur = conn.cursor()
                cur.execute(
                    "SELECT name FROM Blackjack WHERE playing=1")
                results = cur.fetchall()
            if not results:
                self.name = 1
            else:
                results = [x[0] for x in results]
                low = min(results) - 1
                if low > 0:
                    self.name = low
                else:
                    for n in range(low, 1917):
                        if n not in results:
                            self.name = n
                            break

            self.dealt = 0
            self.playing = 1
            self.finished = 0
            self.players = {
                'order'         :   [self.initiator],
                self.house      :   {
                    'name'  :   self.ctx.bot.name,
                    'hand'  :   None,
                    'total' :   0,
                    'bet'   :   None,
                    'status':   True,
                    'turn'  :   False
                },
                self.initiator  :   {
                    'name'  :   self.ctx.message.author.name,
                    'hand'  :   None,
                    'total' :   0,
                    'bet'   :   bet,
                    'status':   True,
                    'turn'  :   False
                }
            }

            with sql.Sql() as conn:
                cur = conn.cursor()
                cur.execute(
                    "INSERT INTO Blackjack VALUES(?, ?, ?, ?, ?, ?, ?, ?)",
                    [self.id, self.name, self.dealt, self.playing,
                     self.finished, self.initiator, str(self.players),
                     str(self.deck)])
                conn.commit()
            check_locked(self.initiator, self.players[self.initiator]['name'],
                         update=self.name)

        else:
            with sql.Sql() as conn:
                cur = cur.cursor()
                self.deck = conn.execute('''
                    SELECT * from Blackjack WHERE playing = 1 AND finished=0
                    AND id=?''', [deck_id])
                results = cur.fetchall()
            try:
                results = results[0]
            except IndexError:
                raise exceptions.DeckError

            self.id = results[0]
            self.name = results[1]
            self.dealt = results[2]
            self.playing = results[3]
            self.finished = results[4]
            self.initiator = results[5]
            self.players = literal_eval(results[6])
            self.deck = literal_eval(results[7])

    def add_player(self, user, bet: int):
        if self.dealt == 1:
            raise exceptions.DealtError
        elif user.id in self.players.keys():
            raise exceptions.PlayerError
        elif len(self.players) >= 7:
            raise exceptions.TableLimit
        elif check_locked(user.id, user.name):
            raise exceptions.PlayerError
        else:
            if bet <= 0:
                raise exceptions.BetError
            self.players[user.id] = {
                'name'  :   user.name,
                'hand'  :   None,
                'total' :   0,
                'bet'   :   bet,
                'status':   True,
                'turn'  :   False
            }
            self.players['order'].append(user.id)
            update(self.id, 'players', str(self.players))
            check_locked(user.id, user.name, update=self.name)

        return self.players[user.id]

    def deal(self):
        if self.dealt == 1:
            raise exceptions.DealtError
        self.deck = random.shuffle(self.deck)
        for player in self.players.keys():
            self.players[player]['hand'] = [self.deck.pop(), self.deck.pop()]
        self.dealt = 1
        update(self.id, ['deck', 'dealt', 'players'],
               [self.deck, self.dealt, str(self.players)])

        return self.players
