#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018-2019 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

class SavoyException(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, *kwargs)

class CommandError(SavoyException):
    def __init__(self, *args, **kwargs):
        SavoyException.__init__(self, *args, *kwargs)

class ArgError(SavoyException):
    def __init__(self, *args, **kwargs):
        SavoyException.__init__(self, *args, *kwargs)

class AttributeError(SavoyException):
    def __init__(self, *args, **kwargs):
        SavoyException.__init__(self, *args, *kwargs)

class PermissionError(SavoyException):
    def __init__(self, *args, **kwargs):
        SavoyException.__init__(self, *args, *kwargs)

class NotFoundError(SavoyException):
    def __init__(self, *args, **kwargs):
        SavoyException.__init__(self, *args, *kwargs)

class GulagError(SavoyException):
    def __init__(self, *args, **kwargs):
        SavoyException.__init__(self, *args, *kwargs)

class BlackjackException(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, *kwargs)

class BetError(BlackjackException):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, *kwargs)

class DeckError(BlackjackException):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, *kwargs)

class PlayerError(BlackjackException):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, *kwargs)

class TableLimit(BlackjackException):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, *kwargs)

class DealtError(BlackjackException):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, *kwargs)
