#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018-2019 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

help = '''
Repository: <https://gitlab.com/savoyroad/savoybot>
```
Welcome to SavoyBot 2.0! Below are the available commands.

For detailed help on a specific command, type ".s <cmd> -h",
replacing <cmd> with any of the below (e.g. .s weather -h).
For text output (as oppossed to embeds), use the "-x" flag (e.g. .s fm -x).

sponge      :   Alternates text capitalization.
clap        :   Inserts clap emoji between words.
set         :   Set user attributes (location, etc.).
weather     :   Shows weather for desired location or user.
fm          :   Shows LastFM information for desired user.
hs          :   Shows daily horoscope for desired sign or user.
save        :   Saves text, image URLs, etc. for retrieval.
get         :   Retrieves a saved item.
tay         :   Outputs a random Taylor picture or one from a specified era.
```'''

sponge = '''```
Outputs the given text with alternating capitalization.

usage:      .s sponge <text>
```'''

clap = '''```
Outputs the given text capitalized and intersped with the clap emoji.

usage:      .s clap <text>
```'''

weather = '''```
Displays the weather for the given location.
The location must either be <zip_code> <2-letter country code> OR
<city> <2-letter country code>. Can also view another user's weather if they
have their location set while no argument will output your set location
(see .s set).

usage:      .s weather [location, user]
options:
            [location]  :   Outputs the weather for the selected location.
            e.g. .s weather Liverpool, GB OR .s weather 613310 RU
            [user]      :   Outputs the weather for the selected user if set.
            e.g. .s weather @User
            If your location is set:
            e.g. .s weather
```'''

fm ='''```
Outputs LastFM user information.
Can also view another user's stats if they have their username set while no
while no argument will output your set username stats (see .s set).

usage:      .s fm [-v, -vv, -vvv, -t, -a] [user]
options:
            [user]      :   Outputs the stats for the selected user.
            e.g. .s fm @User
            e.g. .s fm LastFM_Username
            If your username is set:
            e.g. .s fm

            [-v]    :   Will show a verbose output of previous week stats.
            [-vv]   :   Will show a verbose output of previous month stats.
            [-vvv]  :   Will show a verbose output of overall stats.
            [-t]    :   Track mode for verbose output.
            [-a]    :   Album mode for verbose output.
```'''

hs ='''```
Shows your daily horoscope.

usage:      .s hs [sign, user]
options:
            [sign]      :   Outputs the content of the valid horoscope sign.
            [user]      :   Outputs the content of the user's horoscope sign if
            saved.
            e.g. .s hs @User
            If your username is set:
            e.g. .s hs
```'''

save ='''```
Saves an image or text for quick retrieval with .s get.

usage:      .s save <name> <save>
options:
            <name>      :   The name of the saved item.
            <save>      :   The item to be saved.
```'''

get ='''```
Retrieves an image or text saved with .s save.

usage:      .s get [-m] <name>
options:
            <name>      :   The name of the saved item.
            [-m]        :   View meta information on that given item.
```'''

mg ='''```
Management of saved items. Can only be used by users in the 'wheel' group.

usage:      .s mg [-l, -a, -d] <save>
options:
            <save>      :   The name of the saved item.
            [-l]        :   View list of pending saves.
            [-a]        :   Approve pending saves.
            [-d]        :   Delete saves.
```'''

gulag ='''```
Management of blocked users. Can only be used by users in the 'wheel' group.
Will also "jail" users with the appropriate server role.

usage:      .s gulag [-l, -a, -r] <user>
options:
            <user>      :   The user mentions.
            [-l]        :   View list of gulaged users.
            [-a]        :   Add users to gulag.
            [-r]        :   Remove users from gulag.
```'''

tay ='''```
Outputs a random Taylor Swift picture or one from the given era.
Incorrect era assignments can also be corrected.

usage:      .s tay [-u] [era, [url]]
options:
            [era]       :   The Taylor era to view.
            [-u]        :   Updates a picture with a corrected era.
            [url]       :   If updating an era, the URL of the picture.
```'''

set = '''```
Sets user attributes for use in commands. Multiple attributes may be setd at
once e.g. .s set -u m -f SavoyRoad.

usage:      .s set [options] <value>
options:
            [-l]    :   Sets your location for use in the 'weather' command.
            Must be a combination of either <zip_code> <2-letter country code>
            OR <city> <2-letter country code>.
            e.g. .s set -l Liverpool GB
            [-u]    :   Sets your preferred unit of measurement. Every user is
            defaulted to metric.
            Choices:
                    m: metric
                    i: imperial
                    k: kelvin
            [-f]    :   Sets your LastFM username
            [-s]    :   Sets your horoscope sign
```'''

avi ='''```
Shows user avatar.

usage:      .s avi [user]
options:
            [user]      :   Outputs the avi for the selected user.
            e.g. .s fm @User
```'''

send ='''```
Send DMs through SavoyBot. Can only be used by users in the 'wheel' group.

usage:      .s send -user [<user>..] -message <message>
options:
            -user       :   The user mentions to send to (or mailer).
            -message    :   The message to send.
```'''
purge ='''```
Bans list of users

usage:      .s purge <user>
options:
            <user>      :   The user mentions.
```'''
