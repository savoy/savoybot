#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018-2019 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

import discord
from discord.ext import commands
from base import exceptions
from base import output


async def send(ctx, bot, out, p):
    # Special case to send DM if send() command
    if 'mailer' in p.parsed['-user']:
        p.parsed['-user'] = p.conf['mailer']
    for key, u in enumerate(p.parsed['-user']):
        user = bot.get_user(int(u))
        p.mentions.append(user)
    for m in p.mentions:
        await m.send(p.parsed['-message'])

async def gulag(ctx, bot, out, p):
    server = ctx.guild
    gulag = server.get_role(p.conf['server'][server.id]['gulag'])
    if p.parsed['-a']:
        for user in p.mentions:
            await user.remove_roles(gulag)
    elif p.parsed['-r']:
        for user in p.mentions:
            await user.add_roles(gulag)

async def purge(ctx, bot, out, p):
    for user in p.mentions:
        await user.ban()
