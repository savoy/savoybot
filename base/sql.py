#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018-2019 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If NOT, see <http://www.gnu.org/licenses/>.

import sqlite3
from pathlib import Path
import __main__

class Sql:
    def __init__(self):
        self.conn = sqlite3.connect(
            (Path(__main__.__file__).resolve().parent / 'data.db').as_posix(),
            detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES
        )
        cur = self.conn.cursor()

        cur.execute('''
                CREATE TABLE IF NOT EXISTS Attributes(
                id TEXT PRIMARY KEY NOT NULL,
                name TEXT NOT NULL,
                status INTEGER DEFAULT 0,
                unit TEXT DEFAULT 'm',
                location TEXT,
                lastfm TEXT,
                sign TEXT,
                points INTEGER DEFAULT 0,
                special INTEGER DEFAULT 0,
                locked INTEGER DEFAULT 0);
                ''')
        self.conn.commit()
        cur.execute('''
                CREATE TABLE IF NOT EXISTS Tay(
                files TEXT NOT NULL,
                dt DATE NOT NULL,
                era TEXT NOT NULL,
                correction TEXT);
                ''')
        self.conn.commit()
        cur.execute('''
                CREATE TABLE IF NOT EXISTS TayP(
                files TEXT NOT NULL,
                dt DATE NOT NULL,
                era TEXT NOT NULL,
                correction TEXT);
                ''')
        self.conn.commit()
        cur.execute('''
                CREATE TABLE IF NOT EXISTS Saves(
                save TEXT PRIMARY KEY NOT NULL,
                item TEXT NOT NULL,
                id TEXT NOT NULL,
                name TEXT NOT NULL,
                date date NOT NULL,
                status INTEGER NOT NULL DEFAULT 1);
                ''')
        self.conn.commit()
        cur.execute('''
                CREATE TABLE IF NOT EXISTS Quotes(
                quote TEXT NOT NULL,
                keyword TEXT NOT NULL);
                ''')
        self.conn.commit()
        cur.execute('''
                CREATE TABLE IF NOT EXISTS Blackjack(
                id INTEGER PRIMARY KEY NOT NULL,
                name INTEGER NOT NULL,
                dealt INTEGER NOT NULL DEFAULT 0,
                playing INTEGER NOT NULL DEFAULT 1,
                finished INTEGER NOT NULL DEFAULT 0,
                initiator INTEGER NOT NULL,
                players TEXT NOT NULL,
                deck TEXT NOT NULL);
                ''')
        self.conn.commit()
        cur.close()

    def __enter__(self):
        return self.conn

    def __exit__(self, type, value, traceback):
        self.conn.close()
