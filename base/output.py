#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018-2019 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

import discord
import re

class Output():
    def __init__(self, parsed: dict, data: dict):
        self.data = data
        self.parsed = parsed

        if self.parsed['error']:
            self.color = 0xcc6666
        else:
            self.color = 0xb5bd68

        if not self.parsed['out']:
            self.out = discord.Embed(color=self.color)
            self.out.set_author(
                name=self.parsed['user'].name + '#' + self.parsed['user'].discriminator,
                icon_url=self.parsed['user'].avatar_url)
            try:
                self.out.set_thumbnail(url=self.data['icon'])
            except KeyError:
                pass

        else:
            self.out = (
                "```\n"
                f"{self.parsed['user'].name}#"
                f"{self.parsed['user'].discriminator}\n\n"
            )

    def error(self):
        if type(self.out) == str:
            self.out = (
                f"{self.out}{self.data['header']}\n"
                f"{self.data['value']}\n```")
        else:
            self.out.add_field(
                name=self.data['header'],
                value=self.data['value'],
                inline=True
            )

        return self.out

    def weather(self):
        value = (
            f"{self.data['status']} - {self.data['details']}\n"
            f"{self.data['temp']}°/{self.data['temp2']}°\n"
            f"Humidity: {self.data['humidity']}\n"
            f"Precipitation: {self.data['prep']} {self.data['pre_desc']}\n"
            f"Clouds: {self.data['clouds']}\n"
            f"Wind: {self.data['wind']}\n")
        footer = (
            f"{self.data['city']} {self.data['country']} @ "
            f"{self.data['time']} GMT")

        if type(self.out) == str:
            self.out = self.out + value + '\n' + footer + '\n```'
        else:
            self.out.add_field(
                name=f"{self.data['status']} - {self.data['details']}",
                value=value,
                inline=True
            )
            self.out.set_footer(
                text=footer)

        return self.out

    def set(self):
        value = 'User attributes have been set.'
        if type(self.out) == str:
            self.out = self.out + value + '\n```'
        else:
            self.out.add_field(
                name='Congrats!',
                value=value,
                inline=True
            )

        return self.out

    def fm(self):
        header = self.data['status']
        value = self.data['playing']
        try:
            footer = (
                f"{self.data['lastfm']} - {self.data['total']}")
        except KeyError:
            footer = (
                f"{self.data['lastfm']}")


        if type(self.out) == str:
            try:
                self.out = (
                    f"{self.out}{header}\n"
                    f"{value}\n\n"
                    f"{self.data['period']}\n{self.data['recent']}\n\n"
                    f"{footer}\n```")
            except KeyError:
                self.out = (
                    f"{self.out}{header}\n"
                    f"{value}\n\n"
                    f"{footer}\n```")
        else:
            self.out.add_field(
                name=f"{self.data['status']}",
                value=value,
                inline=False
            )
            try:
                self.out.add_field(
                    name=self.data['period'],
                    value=self.data['recent'],
                    inline=False)
            except KeyError:
                pass
            self.out.set_footer(
                text=footer)

        return self.out

    def hs(self):
        if type(self.out) == str:
            self.out = (
                f"{self.out}{self.data['date']}\n"
                f"{self.data['text']}\n\n"
                f"{self.data['sign']}\n```")
        else:
            self.out.add_field(
                name=self.data['date'],
                value=self.data['text'],
                inline=True
            )
            self.out.set_footer(text=self.data['sign'])

        return self.out

    def save(self):
        if type(self.out) == str:
            self.out = self.out + self.data['value'] + '\n```'
        else:
            self.out.add_field(
                name='Congrats!',
                value=self.data['value'],
                inline=True
            )

        return self.out

    def get(self):
        if type(self.out) == str:
            if self.parsed['-m']:
                self.out = (
                    f"`{self.out[4:-2]}`\n"
                    f"`{self.data['save']}`\n"
                    f"`Saved by: {self.data['name']}`\n"
                    f"`Saved on: {self.data['date']}`\n"
                    f"{self.data['item']}")
            else:
                self.out = (
                    f"`{self.out[4:-2]}`\n"
                    f"{self.data['item']}")
        else:
            self.out.set_image(url=self.data['item'])
            if self.parsed['-m']:
                self.out.add_field(
                    name=self.data['save'],
                    value=(
                        f"Saved by: {self.data['name']}\n"
                        f"Saved on: {self.data['date']}"),
                        inline=True
                )

        return self.out

    def mg(self):
        if self.parsed['-l']:
            header = 'Pending'
            value = ''
            for tup in self.data['results']:
                value = (
                    value + f"{tup[2]} by {tup[1]} @ {tup[0]}:\n" +
                    f"{tup[3]}\n")
        else:
            if self.parsed['-a']:
                header = 'Approved'
                value = self.data['value']
            elif self.parsed['-d']:
                header = 'Deleted'
                value = self.data['value']

        if type(self.out) == str:
            self.out = self.out + header + '\n' + value + '\n```'
        else:
            self.out.add_field(
                name=header,
                value=value,
                inline=True
            )

        return self.out
    def gulag(self):
        value = f"{self.data['results']}"
        header = self.data['header']
        if type(self.out) == str:
            self.out = self.out + header + '\n' + value + '\n```'
        else:
            self.out.add_field(
                name=header,
                value=value,
                inline=True
            )

        return self.out

    def tay(self):
        header = self.data['header']
        value = self.data['value']

        if self.parsed['-u']:
            if type(self.out) == str:
                self.out = self.out + header + '\n' + value + '\n```'
            else:
                self.out.add_field(
                    name=header,
                    value=value,
                    inline=True
                )
        else:
            print(self.data)
            print(value)
            title = re.findall(r'albums/(.+)/\w+\.\w+$',
                               value.replace('%20', ' '))[0]
            footer = f"{self.data['count']}/{self.data['total']} images left"
            if type(self.out) == str:
                self.out = header + '\n' + value
            else:
                self.out.add_field(
                    name=header,
                    value=title,
                    inline=True
                )
                self.out.set_image(url=value)
                self.out.set_footer(text=footer)

        return self.out

    def avi(self):
        if type(self.out) == str:
            self.out = self.data['avi']
        else:
            self.out.set_image(url=self.data['avi'])

        return self.out

    def send(self):
        value = self.data['value']
        header = self.data['header']
        if type(self.out) == str:
            self.out = self.out + header + '\n' + value + '\n```'
        else:
            self.out.add_field(
                name=header,
                value=value,
                inline=True
            )

        return self.out

    def purged(self):
        value = self.data['value']
        header = self.data['header']
        if type(self.out) == str:
            self.out = self.out + header + '\n' + value + '\n```'
        else:
            self.out.add_field(
                name=header,
                value=value,
                inline=True
            )

        return self.out
